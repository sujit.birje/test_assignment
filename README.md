Instructions to use the database

1 . Install your database, use file Design/voting.sql - import this file in your mysql server

2. Copy folder zip to www folder of your server and extract it.
	This will change depend on what server(WAMP, XAMP, LAMP) your using

3. Config Change
	a. open ./application/config/config.php and change base_url variable
		it should be <domain>/<path to folder u kept code>

	b. Change Database configuration
		Open ./application/config/database.php
		Change hostname, username, password accordingly

You are ready to use

Admin Credentials
email : sujit@gmail.com
password : 123

Example Link would be
http://localhost/sujit/admin
http://localhost/sujit/login